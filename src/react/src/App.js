import React, {Component} from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import TransactionAction from "./TransactionAction";
import TransactionsList from "./transactions/TransactionsList"

import axios from 'axios';

import "./App.css";

const API = "http://localhost:8080/api/";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentAmount: 0,
            amount: 0,
            transactionDescription: "",
            transactions: []
        };

        this.amountChangeHandler = this.amountChangeHandler.bind(this);
        this.descriptionChangeHandler = this.descriptionChangeHandler.bind(this);
        this.debitHandler = this.debitHandler.bind(this);
        this.creditHandler = this.creditHandler.bind(this);
    }

    componentDidMount() {
        this.loadTransactions();
    }

    amountChangeHandler(event) {
        this.setState({amount: event.target.value});
    }

    descriptionChangeHandler(event) {
        this.setState({transactionDescription: event.target.value});
    }

    resetState(){
        this.setState({
            amount:0,
            transactionDescription: ""
        });
    }

    getOperationModel(type){
        return {
            amount: this.state.amount,
            type: type,
            description: this.state.transactionDescription
        };
    }

    debitHandler() {
        this.sendTransaction("DEBIT");
    }

    creditHandler() {
        this.sendTransaction("CREDIT");
    }

    sendTransaction(type){
        let model = this.getOperationModel(type);
        axios({
            method: 'POST',
            url: API + 'transaction',
            headers: {'accept': 'application/json','content-type': 'application/json'},
            data: model
        }).then((response) => {
            this.resetState();
            this.setState({
                transactions: [response.data.data].concat(this.state.transactions),
                currentAmount: response.data.currentAmount
            });
        }).catch((err) => {
            console.log("err", err.message);
        });
    }

    loadTransactions(){
        axios({
            method: 'GET',
            url: API + 'transactions',
            headers: {'accept': 'application/json','content-type': 'application/json'},
            data: {}
        }).then((response) => {
            this.setState({
                transactions: response.data.data,
                currentAmount: response.data.currentAmount
            });
        }).catch((err) => {
            console.log("err", err.message);
        });
    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="App-container">
                    <TransactionAction
                        amount={this.state.amount}
                        onAmountChange={this.amountChangeHandler}
                        description={this.state.transactionDescription}
                        onDescriptionChange={this.descriptionChangeHandler}
                        onDebit={this.debitHandler}
                        onCredit={this.creditHandler}/>
                    <span>Current application amount: {this.state.currentAmount}</span>
                    <TransactionsList
                        items={this.state.transactions}/>
                </div>

            </MuiThemeProvider>
        );
    }
}

export default App;
