import React from "react";
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow} from "material-ui/Table";
import TransactionListItem from "./TransactionListItem";

import PropTypes from "prop-types";

const TransactionsList = ( ({items}) => {
    let transactions = items.map((item, index) => {
        return (<TransactionListItem key={index}  item={item}></TransactionListItem>);
    });
    return (
        <div>
            <Table className="App-table">
                <TableHeader adjustForCheckbox={false}
                             displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn>Date</TableHeaderColumn>
                        <TableHeaderColumn>Amount</TableHeaderColumn>
                        <TableHeaderColumn>Description</TableHeaderColumn>
                        <TableHeaderColumn>Type</TableHeaderColumn>
                        <TableHeaderColumn>Status</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody>
                    {transactions}
                </TableBody>
            </Table>
        </div>
    );
});

TransactionsList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
};

export default TransactionsList;