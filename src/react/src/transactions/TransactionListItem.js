import React from "react";
import TableRow from "material-ui/Table/TableRow"
import TableRowColumn from "material-ui/Table/TableRowColumn"
import '../App.css'

const TransactionListItem = ( ({item}) => {
    let date = new Date(item.dateTime).toLocaleString();
    //TODO move style to css after material-ui bug fix
    let color = item.status ?
        (item.type === "DEBIT" ? "#D8FCD8" : "#E6F0FC") : '#FCD8D9';
    return (
        <TableRow style={{backgroundColor: color}}>
            <TableRowColumn>{date}</TableRowColumn>
            <TableRowColumn>{item.amount}</TableRowColumn>
            <TableRowColumn>{item.description}</TableRowColumn>
            <TableRowColumn>{item.type}</TableRowColumn>
            <TableRowColumn>{item.status ? 'Success' : 'Failed'}</TableRowColumn>
        </TableRow>
    );
});

export default TransactionListItem;