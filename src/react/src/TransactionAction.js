import React from "react";
import TextField from "material-ui/TextField"
import RaisedButton from "material-ui/RaisedButton"

const TransactionAction = ( ({amount, description, onAmountChange, onDescriptionChange, onDebit, onCredit}) => {
    return (
        <div>
            <TextField
                name="amountField"
                className="App-count-text"
                type="number"
                onChange={onAmountChange}
                value={amount}/>
            <TextField
                name="descriptionField"
                hintText="Description"
                type="text"
                onChange={onDescriptionChange}
                value={description}/>
            <RaisedButton
                label="Debit"
                onClick={onDebit}
                className="App-button"
                disabled={amount === 0}/>
            <RaisedButton
                label="Credit"
                onClick={onCredit}
                className="App-button"
                disabled={amount === 0}/>
        </div>
    );
});

export default TransactionAction;