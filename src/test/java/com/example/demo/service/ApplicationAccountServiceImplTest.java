package com.example.demo.service;

import com.example.demo.model.Transaction;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ApplicationAccountServiceImplTest {

    @Autowired
    private ApplicationAccountService service;

    @After
    public void removeAll(){
        service.removeAllTransactions();
        service.resetAccount();
    }

    @Test
    public void getCurrentApplicationAccount() throws Exception {
        assertNotNull(service.getCurrentApplicationAccount());
    }

    @Test
    public void createTransaction() throws Exception {
        String description = "TEST_DESCRIPTION";

        //DEBIT
        String type = "DEBIT";
        Transaction transaction = service.createTransaction(BigDecimal.valueOf(5), type, description);
        assertNotNull(transaction);
        assertNotNull(transaction.getId());
        assertEquals(transaction.getAmount(), BigDecimal.valueOf(5));
        assertEquals(transaction.getDescription(), description);
        assertEquals(transaction.getType(), type);
        assertTrue(transaction.getStatus());
        assertNotNull(transaction.getCreationDate());
        assertEquals(service.getCurrentApplicationAccount().getAmount(),
                BigDecimal.valueOf(5).setScale(2, RoundingMode.HALF_UP));

        // CREDIT
        type = "CREDIT";
        transaction = service.createTransaction(BigDecimal.valueOf(5), type, description);
        assertNotNull(transaction);
        assertNotNull(transaction.getId());
        assertEquals(transaction.getAmount(), BigDecimal.valueOf(5));
        assertEquals(transaction.getDescription(), description);
        assertEquals(transaction.getType(), type);
        assertTrue(transaction.getStatus());
        assertNotNull(transaction.getCreationDate());
        assertEquals(service.getCurrentApplicationAccount().getAmount(),
                BigDecimal.valueOf(0).setScale(2, RoundingMode.HALF_UP));

        //CREDIT FAIL
        transaction = service.createTransaction(BigDecimal.valueOf(5), type, description);
        assertNotNull(transaction);
        assertNotNull(transaction.getId());
        assertEquals(transaction.getAmount(), BigDecimal.valueOf(5));
        assertEquals(transaction.getDescription(), description);
        assertEquals(transaction.getType(), type);
        assertFalse(transaction.getStatus());
        assertNotNull(transaction.getCreationDate());
        assertEquals(service.getCurrentApplicationAccount().getAmount(),
                BigDecimal.valueOf(0).setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void getTransactions() throws Exception {
        String description = "TEST_DESCRIPTION";
        String type = "DEBIT";
        Transaction transaction = service.createTransaction(BigDecimal.valueOf(5), type, description);
        type = "CREDIT";
        transaction = service.createTransaction(BigDecimal.valueOf(5), type, description);
        type = "CREDIT";
        description = "SHOULD BE FIRST";
        transaction = service.createTransaction(BigDecimal.valueOf(5), type, description);

        List<Transaction> transactions = service.getTransactions();
        assertEquals(transactions.size(), 3);
        assertEquals(transactions.get(0).getDescription(), description);
        assertFalse(transactions.get(0).getStatus());
    }
}