package com.example.demo.controller;

import com.example.demo.model.Transaction;
import com.example.demo.service.ApplicationAccountService;
import com.example.demo.vo.ApiResponse;
import com.example.demo.vo.TransactionCreateVO;
import com.example.demo.vo.TransactionListVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AmountController {

    @Autowired
    private ApplicationAccountService service;

    @RequestMapping(value = "/transactions", method = RequestMethod.GET)
    ResponseEntity<ApiResponse<List<TransactionListVO>>> getTransactions() {
        List<TransactionListVO> list =
                service.getTransactions()
                        .stream()
                        .map(TransactionListVO::new)
                        .collect(Collectors.toList());
        ApiResponse<List<TransactionListVO>> response =
                new ApiResponse<>(list, service.getCurrentApplicationAccount().getAmount());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    ResponseEntity<ApiResponse<TransactionListVO>> createTransaction(@RequestBody TransactionCreateVO createVO) {
        if(createVO.isValid()){
            Transaction transaction = service.createTransaction(createVO.getAmount(),
                    createVO.getType(), createVO.getDescription());
            ApiResponse<TransactionListVO> response =
                    new ApiResponse<>(new TransactionListVO(transaction), service.getCurrentApplicationAccount().getAmount());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
