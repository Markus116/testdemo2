package com.example.demo.vo;

import com.example.demo.model.Transaction;

import java.math.BigDecimal;
import java.time.ZoneOffset;

public class TransactionListVO {
    private Long dateTime;
    private String description;
    private BigDecimal amount;
    private String type;
    private Boolean status;

    public TransactionListVO(Transaction transaction) {
        dateTime = transaction.getCreationDate().getTime();
        description = transaction.getDescription();
        amount = transaction.getAmount();
        type = transaction.getType();
        status = transaction.getStatus();
    }

    public Long getDateTime() {
        return dateTime;
    }

    public void setDateTime(Long dateTime) {
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
