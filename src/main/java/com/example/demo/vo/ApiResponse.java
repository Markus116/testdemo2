package com.example.demo.vo;

import java.math.BigDecimal;

public class ApiResponse<T> {
    private T data;
    private BigDecimal currentAmount;

    public ApiResponse(T data, BigDecimal currentAmount) {
        this.data = data;
        this.currentAmount = currentAmount;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }
}
