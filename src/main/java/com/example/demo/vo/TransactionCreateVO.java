package com.example.demo.vo;

import java.math.BigDecimal;

public class TransactionCreateVO {
    private BigDecimal amount;
    private String type;
    private String description;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isValid(){
        return amount != null && type != null;
    }
}
