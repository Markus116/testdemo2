package com.example.demo.repository;

import com.example.demo.model.ApplicationAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationAccountRepository extends JpaRepository<ApplicationAccount, Long>{
}
