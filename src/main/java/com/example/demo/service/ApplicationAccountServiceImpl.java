package com.example.demo.service;

import com.example.demo.model.ApplicationAccount;
import com.example.demo.model.Transaction;
import com.example.demo.repository.ApplicationAccountRepository;
import com.example.demo.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class ApplicationAccountServiceImpl implements ApplicationAccountService {

    @Autowired
    private ApplicationAccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public ApplicationAccount getCurrentApplicationAccount(){
        return accountRepository.findOne(1L);
    }

    @Override
    public void removeAllTransactions() {
        transactionRepository.deleteAll();
    }

    @Override
    public Transaction createTransaction(BigDecimal amount, String type, String description) {
        ApplicationAccount account = getCurrentApplicationAccount();
        Transaction transaction = new Transaction(amount, type, description);
        transaction.setAccount(getCurrentApplicationAccount());

        if(account.getAmount().compareTo(amount) != -1 || type.equals("DEBIT")){
            transaction.setStatus(true);
            if(type.equals("DEBIT")){
                account.setAmount(account.getAmount().add(amount));
            } else {
                account.setAmount(account.getAmount().subtract(amount));
            }
            accountRepository.saveAndFlush(account);
        } else {
            transaction.setStatus(false);
        }
        return transactionRepository.saveAndFlush(transaction);
    }

    @Override
    public List<Transaction> getTransactions() {
        return transactionRepository.findAllByAccount_IdOrderByCreationDateDesc(getCurrentApplicationAccount().getId());
    }

    @Override
    public void resetAccount() {
        ApplicationAccount account = getCurrentApplicationAccount();
        account.setAmount(BigDecimal.valueOf(0));
        accountRepository.saveAndFlush(account);
    }
}
