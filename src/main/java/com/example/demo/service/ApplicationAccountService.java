package com.example.demo.service;

import com.example.demo.model.ApplicationAccount;
import com.example.demo.model.Transaction;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

public interface ApplicationAccountService {
    Transaction createTransaction(BigDecimal amount, String type, String description);

    List<Transaction> getTransactions();

    ApplicationAccount getCurrentApplicationAccount();

    void removeAllTransactions();

    void resetAccount();
}
